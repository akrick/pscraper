<?php
namespace Pscraper;

use QL\Ext\PhantomJs;
use QL\QueryList;

class ZwdScraper extends Scraper
{

    /**
     * @var QueryList|null
     */
    private $client;

    public function __construct()
    {
        $this->client = QueryList::getInstance();
        $this->client->use(PhantomJs::class, 'D:\wwwroot\pscraper\libs\phantomjs.exe','browser');
    }

    /**
     * 抓取商品数据
     * @param $url
     * @return array
     */
    public function getGoodsParams($url){
        $params = array();
        try {
            $ql = $this->client->browser($url);
            $params['store_goods_url'] = $url;
            $params['store_name'] = $ql->find("body > div.web-container > div.item-clear-container.main-function > div.promote-goods-page-container > div.goods-shop-info-wrap > div > div.upper-part > div.new-shop-panel-header > div.left")->text();
            $params['store_tel'] = $ql->find("body > div.web-container > div.item-clear-container.main-function > div.promote-goods-page-container > div.goods-shop-info-wrap > div > div.second-part > ul > li:nth-child(2) > div.new-shop-panel-item-value > a")->texts()->toArray();
            $params['store_address'] = $ql->find("body > div.web-container > div.item-clear-container.main-function > div.promote-goods-page-container.is-flagship > div.goods-shop-info-wrap > div > div.second-part > ul > li:nth-child(6) > div.new-shop-panel-item-value.new-shop-panel-item-value-showall")->text();
            $params['goods_name'] = $ql->find("#J_goodsForm > div.goods-item-title > span")->text();
            $params['original_price']= $ql->find("#J_goodsForm > div.goods-parameter-outside-container > div.goods-page-server-container > div.goods-page-top > div > span.goods-value")->text();
            $params['wholesale_price'] = $ql->find("#goods-pifa-price")->text();
            $params['store_goods_no'] = $ql->find("#J_goodsForm > div.goods-parameter-outside-container > div.goods-all-parameter-container > div:nth-child(1) > div.parameter-right > a")->text();
            $params['store_onsale_time'] = $ql->find("#J_goodsForm > div.goods-parameter-outside-container > div.goods-all-parameter-container > div:nth-child(2) > div.parameter-right > a")->text();
            $params['colors'] = $ql->find("#sku-color-selector > span")->attrs("title")->toArray();
            $params['sizes'] = $ql->find("#sku-size-selector > ul > li > div:nth-child(1)")->texts()->toArray();
            $params['size_prices'] = $ql->find("#sku-size-selector > ul > li > div.price")->texts()->toArray();
            $this->priceFilter($params['size_prices']);
            $params['goods_details'] = $ql->find("div.details-right-content")->text();
            $params['goods_details'] = str_replace("\n\n\n", "|", str_replace(" ", "", $params['goods_details']));
            $params['goods_pics'] = $ql->find("body > div.web-container > div.item-clear-container.main-function > div.promote-goods-page-container > div.view-BigPicture > div.BigPicture-content > div.left-area > div.imgWrap > a")->attrs("href")->toArray();
        }catch (\Exception $e){
            $this->dd($e->getTraceAsString());
        }
        return $params;
    }
}