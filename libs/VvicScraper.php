<?php
namespace Pscraper;

use QL\Ext\PhantomJs;
use QL\QueryList;

class VvicScraper extends Scraper
{
    /**
     * @var QueryList|null
     */
    private $client;

    public function __construct()
    {
        $this->client = QueryList::getInstance();
        $this->client->use(PhantomJs::class, 'D:\wwwroot\pscraper\libs\phantomjs.exe','browser');
    }

    public function getGoodsParams($url){
        $params = array();
        try {
            $ql = $this->client->browser($url);
            $params['store_goods_url'] = $url;
            $params['store_name'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fr.item-right.mt20 > div.shop-info > div > h2 > span")->text();
            $params['store_tel'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fr.item-right.mt20 > div.shop-info > div > ul > li.tel-list > div.text > p > span:nth-child(odd)")->texts()->toArray();
            $tels = array();
            $tels[] = implode('', array_slice($params['store_tel'], 0, 11));
            $tels[] = implode('', array_slice($params['store_tel'], 11));
            $params['store_tel'] = $tels;
            $params['store_address'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fr.item-right.mt20 > div.shop-info > div > ul > li:nth-child(8) > div.text")->text();
            $params['store_address'] = str_replace("\n", "", str_replace(" ", "", $params['store_address']));
            $params['goods_name'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fl.item-left.mt20 > div.product-detail > div.d-name > strong")->text();
            $params['original_price'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fl.item-left.mt20 > div.product-detail > div.price-time-buyer > div:nth-child(2) > div.p-value > span.d-sale")->text();
            $params['wholesale_price'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fl.item-left.mt20 > div.product-detail > div.price-time-buyer > div.v-price.d-p > div.p-value > span > strong.d-sale")->text();
            $params['store_goods_no'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fl.item-left.mt20 > div.product-detail > dl.summary.clearfix > dd:nth-child(1) > div.value.ff-arial")->text();
            $params['store_goods_no'] = str_replace("\n", "", str_replace(" ", "", $params['store_goods_no']));
            $params['store_onsale_time'] = $ql->find("body > div.w.clearfix > div.item-content.clearfix > div.fl.item-left.mt20 > div.product-detail > dl.summary.clearfix > dd:nth-child(2) > div.value.ff-arial")->text();
            $params['colors'] = $ql->find("#j-buy > dd:nth-child(2) > div.value > ul > li.selectColor")->attrs("data-color")->toArray();
            $params['sizes'] = $ql->find("#j-buy > dd:nth-child(3) > div.value.goods-choice.goods-choice-size > ul > div > li > span.selectSize")->texts()->toArray();
            $params['size_prices'] = $ql->find("#j-buy > dd:nth-child(3) > div.value.goods-choice.goods-choice-size > ul > div > li > span.skuPrice.skuPrice")->texts()->toArray();
            $params['goods_details'] = $ql->find("#info > div:nth-child(1) > div.d-attr.clearfix > ul")->text();
            $params['goods_details'] = str_replace("\n\n\n\n", "|", str_replace(" ", "", $params['goods_details']));
            $params['goods_pics'] = $ql->find("#thumblist > div.owl-stage-outer > div > div > div > a > img")->attrs("big")->toArray();
        }catch (\Exception $e){
            $this->dd($e->getTraceAsString());
        }
        return $params;
    }
}