<?php
namespace Pscraper;


class Scraper
{
    public function dd($data, $is_dump = false){
        if ($is_dump){
            var_dump($data);
        }else{
            print_r($data);
        }
        exit;
    }

    /**
     * 过滤并格式化价格值
     * @param $prices
     */
    public function priceFilter(&$prices){
        if (!empty($prices)){
            foreach ($prices as $key => $item){
                $prices[$key] = str_replace("￥", "", $item);
            }
        }
    }

    /**
     * 过滤并格式化颜色值
     * @param $colors
     */
    public function colorFilter(&$colors){
        if (!empty($colors)){
            foreach ($colors as $key => $item){
                $colors[$key] = $item;
            }
        }
    }
}