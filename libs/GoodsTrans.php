<?php
namespace Pscraper;
use Pscraper\TransBaidu;

class GoodsTrans
{
    private $trans;
    public function __construct()
    {
        $this->trans = new TransBaidu();
    }

    /**
     * 翻译商品信息
     * @param $params
     * @return mixed
     */
    public function translate(&$params){

        $trans = new TransBaidu();
        $ret = $trans->translate($params['goods_name'], "zh", "en");
//        print_r($ret);exit;
        $params['goods_name_en'] = $ret['trans_result'][0]['dst'];
        $goods_details = explode("|", $params['goods_details']);
        if (!empty($goods_details)){
            $details = array();
            foreach ($goods_details as $item){
                $ret = $trans->translate($item, "auto", "en");
                $details[] = $ret['trans_result'][0]['dst'];
            }
            $params['goods_details_en'] = $details;
        }
        if (!empty($params['colors'])) {
            $colors = array();
            foreach ($params['colors'] as $item) {
                $itemarr = explode("【", $item);
                $ret = $trans->translate($itemarr[0], "auto", "en");
                $colors[] = $ret['trans_result'][0]['dst'];
            }
            $params['colors_en'] = $colors;
        }
        return $params;
    }

}