<?php

use Pscraper\GoodsTrans;
use Pscraper\ZwdScraper;

require_once "init.php";

$url = "https://gz.17zwd.com/item.htm?GID=116576614&spm=3485a93c151bfd98.42.140.0.0.145630.0";
$scraper = new ZwdScraper();
$params = $scraper->getGoodsParams($url);
$goods_trans = new GoodsTrans();
$params = $goods_trans->translate($params);
$scraper->dd($params);